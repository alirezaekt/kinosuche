const usePreferences = () => {

    const currentCity = useState("city",() => 'Köln')
    const preferences = useState("preferences",() => { return {
        //default preferences
        //   "City": 'Köln',
          "Theme": 'system'
        }
    })

    const setCity = ( value) => {
        currentCity.value = value
        const cookie = useCookie("kinocity")
        cookie.value = value
    }
    
    const setPreference = (item, value) => {
        preferences.value[item] = value
    }

    return{
        currentCity,
        setCity,
        preferences,
        setPreference
    }
}
export default usePreferences
