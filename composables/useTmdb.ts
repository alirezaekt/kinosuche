const useTmdb = () => {
    const config = useRuntimeConfig()

    const getCinemaMovies = async (pageNumber:number=1) => {
        const { pending, data:movies } = await useFetch('https://api.themoviedb.org/3/movie/now_playing?language=en-US',{
        params: {
                api_key: config.public.tmdbKey,
                page:pageNumber,
                region:'DE'
            }
        })
        if (movies.value)
            return movies.value['results']
    }
    const getSortedMovies = async (pageNumber:number=1) => {
        const { pending, data:movies } = await useFetch('https://api.themoviedb.org/3/movie/top_rated?language=en-US',{
        params: {
                api_key: config.public.tmdbKey,
                page:pageNumber,
                region:'DE'
            }
        })
        if (movies.value)
            return movies.value['results'].sort((a, b) => (a['release_date'] < b['release_date']) ? 1 : -1)
    }

    const getMovieDetails = async (movieId) => {
        const { pending, data:movie } = await useFetch(`https://api.themoviedb.org/3/movie/${movieId}`,{
        params: {
                api_key: config.public.tmdbKey,
                append_to_response:'videos'
            }
        })
        if (movie.value)
            return movie
    }

    const getMovieCast = async (movieId) => {
        const { pending, data:cast } = await useFetch(`https://api.themoviedb.org/3/movie/${movieId}/credits`,{
        params: {
                api_key: config.public.tmdbKey
            }
        })
        if (cast.value)
            return cast.value
    }

    const getMovieProviders = async (movieId) => {
        const { pending, data:movie } = await useFetch(`https://api.themoviedb.org/3/movie/${movieId}/watch/providers`,{
        params: {
                api_key: config.public.tmdbKey,
            }
        })
        return movie
    }

    const toImageUrl = (posterPath:string) => {
        return 'http://image.tmdb.org/t/p/w300' + posterPath
    }

    const searchMovies = async (query:string) => {
        const { pending, data:results } = await useFetch(`https://api.themoviedb.org/3/search/movie?language=en-US`,{
        params: {
                api_key: config.public.tmdbKey,
                page:1,
                include_adult:false,
                query:query
            }
        })
        return results.value['results']
    }

    //
    const getKinoTimes = async (movie_name,city) => {
        const { pending, data } = await useFetch(config.public.kinoServer + '/kinode_id',{
            params: {
                    name: movie_name,
                    city: encodeURI(city)
                }
            })
        if (data.value === 'NoShow'){
            return 'NoShow'
        } else if (data.value){
            return data.value['data']
        } else {
            return 'NoConnection'
        }
    }
    return {
        getCinemaMovies,
        getSortedMovies,
        getMovieDetails,
        getMovieCast,
        getMovieProviders,
        toImageUrl,
        searchMovies,
        getKinoTimes
    }
}
export default useTmdb