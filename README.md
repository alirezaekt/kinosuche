# KinoSuche
Find cinema showtimes of movies, concerts of your favorite bands, and ticket links.

Check out the [Demo](https://kino.coolconcepts.tech/)

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```


Set the TMDB api key and showtime retrieval backend url in .env file:
```bash
TMDB_KEY=***
KINO_SERVER=***
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```
